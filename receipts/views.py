from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def receipt_list(request):
    list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": list,
    }
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        # we should use the form to validate
        # the values and save them to
        # the database
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
            # if all goes well, we can redirect
            # the browser to another page and
            # leave the function
    else:
        form = ReceiptForm()
            # Create an instance of the Django
            # model form class
    context = {
        "form": form,
        }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": list,
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    list = Account.objects.filter(owner=request.user)
    context = {
        "account_list": list,
    }
    return render(request, "receipts/account_list.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        # we should use the form to validate
        # the values and save them to
        # the database
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
            # if all goes well, we can redirect
            # the browser to another page and
            # leave the function
    else:
        form = ExpenseCategoryForm()
            # Create an instance of the Django
            # model form class
    context = {
        "form": form,
        }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        # we should use the form to validate
        # the values and save them to
        # the database
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
            # if all goes well, we can redirect
            # the browser to another page and
            # leave the function
    else:
        form = AccountForm()
            # Create an instance of the Django
            # model form class
    context = {
        "form": form,
        }
    return render(request, "receipts/create_account.html", context)





# the key(s) in your context of your view is what you'll
# refer to in your html template

# functions can go
# inside out but not outside in
# scope ^
# for example list = inside each function
# is only specific to that function
